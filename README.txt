Module: Piwik Analytics
Sponsored by IO1, http://www.io1.biz


Description
===========
Adds the Piwik Analytics tracking system (www.piwik.org) to your website. 

Requirements
============

1) Web server: httpd-2.x;
2) php 5.1.x minimum; 
3) mysql-5.0.x;
4) Drupal-6.x-1.0;
5) php PDO module enabled
6) Piwik (vers. 2008-04-24)


Installation
============
* Copy the 'piwikanalytics' module directory in to your Drupal modules directory as usual.

Usage
=====
In the settings page enter your Piwik URL, site ID and authentication string.

Also you will need to define what user roles should be tracked.
Simply tick the roles you would like to monitor.

'admin/' pages are automatically ignored by Piwik Analytics.

Piwik Analytics statistics are available in reports menu.
