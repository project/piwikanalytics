<?php

/**
 * Drupal Module: PiwikAnalytics
 * Sponsored by IO1, http://www.io1.biz
 * Adds the required Javascript to the bottom of all your Drupal pages
 * to allow tracking by the Piwik Analytics statistics package.
 * All code is released under the GNU General Public License.
 */

/**
 * @file
 * Administrative page callbacks for the piwikanalytics module.
 */

/**
 * Menu callback; displays the piwikanalytics module settings page. 
 */
function piwikanalytics_admin_settings() {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Piwik Settings'),
    '#collapsible' => FALSE,
  );
  $form['account']['piwikanalytics_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Piwik base URL'),
    '#default_value' => variable_get('piwikanalytics_url', ''),
    '#size' => 60,
    '#maxlength' => 200,
    '#required' => TRUE,
    '#description' => t('Ex: http://piwik.org'),
  );
  $form['account']['piwikanalytics_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('piwikanalytics_site_id', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
  );
  $form['account']['piwikanalytics_auth'] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication string'),
    '#default_value' => variable_get('piwikanalytics_auth', ''),
    '#size' => 40,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Need for request the data without being logged in to Piwik. You can get this token in the Piwik Manage Users admin area.'),
  );

  $result = db_query('SELECT * FROM {role} ORDER BY name');
  $form['roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Role Tracking'),
    '#collapsible' => TRUE,
    '#description' => t('Define what user roles should be tracked by Piwik Analytics. <strong>Note:</strong> Drupal Admin pages are never tracked.'),
  );
  $form['roles']['piwikanalytics_track__user1'] = array(
    '#type' => 'checkbox',
    '#title' => t('Admin (user 1)'),
    '#default_value' => variable_get('piwikanalytics_track__user1', FALSE),
  );
  while ($role = db_fetch_object($result)) {
    $form['roles']['piwikanalytics_track_'. $role->rid] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($role->name),
      '#default_value' => variable_get('piwikanalytics_track_'. $role->rid, FALSE),
    );
  }

  return system_settings_form($form);
}



