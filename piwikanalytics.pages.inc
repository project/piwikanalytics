<?php

/**
 * Drupal Module: PiwikAnalytics
 * Sponsored by IO1, http://www.io1.biz
 * Adds the required Javascript to the bottom of all your Drupal pages
 * to allow tracking by the Piwik Analytics statistics package.
 * All code is released under the GNU General Public License.
 */

/**
 * Report menu links
 */
function piwikanalytics_menu_block_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', $content);
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}

function piwikanalytics_dateselect_form() {
  $period = array(
    0 => "today",
    1 => "yesterday",
    2 => "last week",
    3 => "last month",
    4 => "last year",
  );
  $form["period"] = array(
    '#type' => 'select',
    '#value' => variable_get('piwikanalytics_period', ''),
    '#options' => $period,
    '#prefix' => '<table><tr><td width="80px">'. t('Time period: ') .'</td><td width="100px">',
    '#suffix' => '</td>',
  );
  $form["change"] = array(
    '#type' => 'submit',
    '#value' => t('Change'),
    '#prefix' => '<td>',
    '#suffix' => '</td></tr></table>',
  );
  return $form;
}

function piwikanalytics_overview() {
  $form   = piwikanalytics_dateselect_form();
  $period = variable_get('piwikanalytics_period', '');
  $date   = select_period($period);
  $now    = select_period(0);
  $period = get_period_name($period);
  $auth   = variable_get('piwikanalytics_auth', '');
  $url    = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=VisitsSummary&action=getLastVisitsGraph&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."%2C". $now ."&viewDataTable=generateDataChartEvolution&token_auth=". $auth
  );
  $output = theme('visitors_overview', $url, $period);
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

function piwikanalytics_overview_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_settings() {
  $form = piwikanalytics_dateselect_form();
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period = get_period_name($period);
  $auth   = variable_get('piwikanalytics_auth', '');
  $url    = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=UserSettings&action=getBrowserType&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartPie&token_auth=". $auth
  );
  $url2 = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=UserSettings&action=getConfiguration&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartVerticalBar&token_auth=". $auth
  );
  $url3 = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=UserSettings&action=getOS&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartVerticalBar&token_auth=". $auth
  );
  $url4 = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=UserSettings&action=getResolution&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartVerticalBar&token_auth=". $auth
  );
  $output = theme('visitors_settings', $url, $url2, $url3, $url4);
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

function piwikanalytics_settings_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_times() {
  $form = piwikanalytics_dateselect_form();
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period = get_period_name($period);
  $auth   = variable_get('piwikanalytics_auth', '');
  $url    = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=VisitTime&action=getVisitInformationPerLocalTime&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartVerticalBar&token_auth=". $auth
  );
  $output = theme('visitors_times', $url);
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

function piwikanalytics_times_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_locations() {
  $form = piwikanalytics_dateselect_form();
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period = get_period_name($period);
  $auth   = variable_get('piwikanalytics_auth', '');
  $url    = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=UserCountry&action=getCountry&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."&viewDataTable=generateDataChartPie&token_auth=". $auth
  );
  $output = theme('visitors_locations', $url);
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

function piwikanalytics_locations_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_pages() {
  drupal_add_css(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.css');
  drupal_add_js('misc/jquery.js');
  $form   = piwikanalytics_dateselect_form();
  $auth   = variable_get('piwikanalytics_auth', '');
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period       = get_period_name($period);
  $url          = variable_get('piwikanalytics_url', '') ."/index.php?module=API&method=Actions.getActions&idSite=". variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."&format=JSON&token_auth=". $auth ."&filter_sort_column=nb_uniq_visitors&filter_sort_order=desc";
  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => 'actions',
  );
  $form['url'] = array(
    '#type' => 'hidden',
    '#value' => $url,
  );
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => '<div id="pagestable"></div>',
  );
  drupal_add_js(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.js');
  return $form;
}

function piwikanalytics_pages_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_outlinks() {
  drupal_add_css(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.css');
  drupal_add_js('misc/jquery.js');
  $form   = piwikanalytics_dateselect_form();
  $auth   = variable_get('piwikanalytics_auth', '');
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period       = get_period_name($period);
  $url          = variable_get('piwikanalytics_url', '') ."/index.php?module=API&method=Actions.getOutlinks&idSite=". variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."&format=JSON&token_auth=". $auth ."&filter_sort_column=nb_uniq_visitors&filter_sort_order=desc";
  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => 'actions',
  );
  $form['url'] = array(
    '#type' => 'hidden',
    '#value' => $url,
  );
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => '<div id="pagestable"></div>',
  );
  drupal_add_js(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.js');
  return $form;
}

function piwikanalytics_outlinks_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_downloads() {
  drupal_add_css(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.css');
  drupal_add_js('misc/jquery.js');
  $form   = piwikanalytics_dateselect_form();
  $auth   = variable_get('piwikanalytics_auth', '');
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period       = get_period_name($period);
  $url          = variable_get('piwikanalytics_url', '') ."/index.php?module=API&method=Actions.getDownloads&idSite=". variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."&format=JSON&token_auth=". $auth ."&filter_sort_column=nb_uniq_visitors&filter_sort_order=desc";
  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => 'actions',
  );
  $form['url'] = array(
    '#type' => 'hidden',
    '#value' => $url,
  );
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => '<div id="pagestable"></div>',
  );
  drupal_add_js(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.js');
  return $form;
}

function piwikanalytics_downloads_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_evolution() {
  $form   = piwikanalytics_dateselect_form();
  $period = variable_get('piwikanalytics_period', '');
  $date   = select_period($period);
  $now    = select_period(0);
  $period = get_period_name($period);
  $auth   = variable_get('piwikanalytics_auth', '');
  $url    = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=Referers&action=getLastDirectEntryGraph&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=".
    $period ."&date=". $date ."%2C". $now ."&viewDataTable=generateDataChartEvolution&token_auth=". $auth
  );
  $output = theme('referers_evolution', $url);
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  return $form;
}

function piwikanalytics_evolution_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_search() {
  drupal_add_css(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.css');
  drupal_add_js('misc/jquery.js');
  $form   = piwikanalytics_dateselect_form();
  $auth   = variable_get('piwikanalytics_auth', '');
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period = get_period_name($period);
  $url = variable_get('piwikanalytics_url', '') ."/libs/open-flash-chart/open-flash-chart.swf?data=". urlencode(variable_get('piwikanalytics_url', '') ."/?module=Referers&action=getSearchEngines&idSite=".
    variable_get('piwikanalytics_site_id', '') ."&period=". $period .
    "&date=yesterday&viewDataTable=generateDataChartPie&token_auth=". $auth
  );
  $output = theme('referers_search', $url);
  $url2 = variable_get('piwikanalytics_url', '') ."/index.php?module=API&method=Referers.getKeywords&idSite=". variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."&format=JSON&token_auth=". $auth ."&filter_sort_column=nb_visits&filter_sort_order=desc";

  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => 'search',
  );
  $form['url'] = array(
    '#type' => 'hidden',
    '#value' => $url2,
  );
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => $output,
  );
  $form['tablecontent'] = array(
    '#type' => 'markup',
    '#value' => '<div id="pagestable"></div>',
  );
  drupal_add_js(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.js');
  return $form;
}

function piwikanalytics_search_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function piwikanalytics_websites() {
  drupal_add_css(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.css');
  drupal_add_js('misc/jquery.js');
  $form   = piwikanalytics_dateselect_form();
  $auth   = variable_get('piwikanalytics_auth', '');
  $period = variable_get('piwikanalytics_period', '');
  ($period == 1) ? $date = select_period($period) : $date = $date = select_period(0);
  $period      = get_period_name($period);
  $url         = variable_get('piwikanalytics_url', '') ."/index.php?module=API&method=Referers.getWebsites&idSite=". variable_get('piwikanalytics_site_id', '') ."&period=". $period ."&date=". $date ."&format=JSON&token_auth=". $auth;
  $form['url'] = array(
    '#type' => 'hidden',
    '#value' => $url,
  );
  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => 'websites',
  );
  $form['content'] = array(
    '#type' => 'markup',
    '#value' => '<div id="pagestable"></div>',
  );
  drupal_add_js(drupal_get_path('module', 'piwikanalytics') .'/piwikanalytics.js');
  return $form;
}

function piwikanalytics_websites_submit($form, &$form_state) {
  variable_set('piwikanalytics_period', $form_state['clicked_button']['#post']['period']);
}

function select_period($period) {
  switch ($period) {
    case 0:
      $date = date("Y-m-d");
      break;

    case 1:
      $d = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 2:
      $d = mktime(0, 0, 0, date("m"), date("d") - 7, date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 3:
      $d = mktime(0, 0, 0, date("m") - 2, date("d"), date("Y"));
      $date = date("Y-m-d", $d);
      break;

    case 4:
      $d = mktime(0, 0, 0, date("m"), date("d"), date("Y") - 1);
      $date = date("Y-m-d", $d);
      break;
  }
  return $date;
}

function get_period_name($period) {
  switch ($period) {
    case 0:
      $p = "day";
      break;

    case 1:
      $p = "day";
      break;

    case 2:
      $p = "week";
      break;

    case 3:
      $p = "month";
      break;

    case 4:
      $p = "year";
      break;
  }
  return $p;
}

